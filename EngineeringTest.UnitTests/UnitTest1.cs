﻿using System;
using EngineeringTest.PhysicsEngine;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EngineeringTest.UnitTests
{
    [TestClass]
    public class RectangleTest
    {
        [TestMethod]
        public void IsPointInside_PointIsInside_ReturnsTrue()
        {
            var _r1 = new Rectangle(3.0f,3.0f,5.0f,5.0f);

            var _result = _r1.IsPointInside(3.0f, 3.0f);

            Assert.IsTrue(_result);                    
        }

        [TestMethod]
        public void IsPointInside_PointIsNotInside_ReturnsFalse()
        {
            var _r1 = new Rectangle(3.0f, 3.0f, 5.0f, 5.0f);

            var _result = _r1.IsPointInside(50.0f, 50.0f);

            Assert.IsFalse(_result);
        }
        
        [TestMethod]
        public void AssignmentOperator_RectanglesAreTheSame_ReturnsTrue()
        {
            var _r1 = new Rectangle(3.0f, 3.0f, 5.0f, 5.0f);
            var _r2 = new Rectangle();

            _r2 = _r1;

            Assert.AreEqual(_r1, _r2);
        }

        [TestMethod]
        public void EqualityOperator_SizeOfRectanglesAreEqual_ReturnsTrue()
        {
            var _r1 = new Rectangle(3.0f, 3.0f, 5.0f, 5.0f);
            var _r2 = new Rectangle(_r1);

            var _result = _r1 == _r2;

            Assert.IsTrue(_result);
        }

        [TestMethod]
        public void EqualityOperator_SizeOfRectanglesAreInequal_ReturnsFalse()
        {
            var _r1 = new Rectangle(3.0f, 3.0f, 5.0f, 5.0f);
            var _r2 = new Rectangle(3.0f, 3.0f, 10.0f, 5.0f);

            var _result = _r1 == _r2;

            Assert.IsFalse(_result);
        }

        [TestMethod]
        public void InequalityOperator_SizeOfRectanglesAreInequal_ReturnsTrue()
        {
            var _r1 = new Rectangle(3.0f, 3.0f, 5.0f, 5.0f);
            var _r2 = new Rectangle(3.0f, 3.0f, 10.0f, 5.0f);

            var _result = _r1 != _r2;

            Assert.IsTrue(_result);
        }

        [TestMethod]
        public void InequalityOperator_SizeOfRectanglesAreEqual_ReturnsFalse()
        {
            var _r1 = new Rectangle(3.0f, 3.0f, 5.0f, 5.0f);
            var _r2 = new Rectangle(_r1);

            var _result = _r1 != _r2;

            Assert.IsFalse(_result);
        }
    }
}
