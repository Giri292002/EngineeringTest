﻿using System;
using EngineeringTest.PhysicsEngine;

class TestProgram
{
    public static void Main()
    {
        //Rectangle 1
        Rectangle _r1 = new Rectangle(3.0f, 3.0f, 5.0f, 5.0f); //Construct Rectangle with Parameters

        //Rectangle 2 made with a Copy Constructor
        Rectangle _r2 = new Rectangle(_r1);

        // Rectangle 3
        Rectangle _r3 = new Rectangle(3.0f, 3.0f, 20.0f, 20.0f);

        //Circle 1
        Circle _c1 = new Circle(0.0f, 0.0f, 5.0f);

        //Check if rectangle sizes are same using the Equality operator
        Console.Write($"\n\n Checking Size for : {_r1.ToString()} \nAND\n {_r2.ToString()}");
        if (_r2 == _r1)
        {
            Console.Write($"\n\nSizes are same");
        }
        else
        {
            Console.Write("\n\nSizes are different");
        }

        _r2 = _r3; // Use Assignment Operator to make _r2 the same as _r3, 
                   //c# doesn't support assignment operator overload since it automatically manages memory


        //Check if rectangle sizes are same using the Equality operator
        Console.Write($"\n\n Checking Size for : {_r2.ToString()} \nAND\n {_r3.ToString()}");

        if (_r2 == _r3)
        {
            Console.Write("\n\nSizes are same");
        }
        else
        {
            Console.Write("\n\nSizes are different");
        }

        Console.Write($"\n\n Checking If Point is in {_r1.ToString()}");

        //Check if a given point is in bound
        if (_r1.IsPointInside(15.0f, 3.0f))
        {
            Console.Write("\nPoint In Bound");
        }
        else
        {
            Console.Write("\nPoint is not in bound");
        }

        Console.WriteLine("\n\nPress any key to exit.");
        Console.ReadKey();
    }
}

