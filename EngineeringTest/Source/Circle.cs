namespace EngineeringTest.PhysicsEngine
{

    public class Circle : Shape
    {
        public float Radius; //Radius of the circle

        public Circle(float posX, float posY, float radius) : base(posX, posY)
        {
            PosX = posX;
            PosY = posY;
            Radius = radius;
        }
    }
}