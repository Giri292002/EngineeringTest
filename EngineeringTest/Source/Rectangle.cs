namespace EngineeringTest.PhysicsEngine
{
    public class Rectangle : Quadrilateral
    {
        public float Height; //Height of the rectangle
        public float Width; //Width of the rectangle    

        //Default Constructor
        public Rectangle() : base(0, 0)
        {
            Height = 1f;
            Width = 1f;
        }

        //Parameterized Constructor
        public Rectangle(float posX, float posY, float height, float width) : base(posX, posY)
        {
            Height = height;
            Width = width;
        }

        //Copy Constructor
        public Rectangle(Rectangle r1) : base(r1.PosX, r1.PosY)
        {

            PosX = r1.PosX;
            PosY = r1.PosY;

            Width = r1.Width;
            Height = r1.Height;
        }

        //Check if the point is in bound
        public bool IsPointInside(float x, float y)
        {
            return x < this.PosX + this.Width &&
                   y < this.PosY + this.Height;
        }

        // Override ==
        public static bool operator ==(Rectangle r1, Rectangle r2)
        {
            return (r1.Height == r2.Height) && (r1.Width == r2.Width);
        }

        //Override !=
        public static bool operator !=(Rectangle r1, Rectangle r2)
        {
            return (r1.Height != r2.Height) || (r1.Width != r2.Width);
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            return Equals(obj as Rectangle);
        }

        public bool Equals(Rectangle r1)
        {
            return r1.Height == this.Height && r1.Width == this.Width;
        }

        // Override HashCode
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"\nPosition: ({PosX},{PosY}) \nWidth: {Width} \nHeight : {Height}";
        }
    }


}
