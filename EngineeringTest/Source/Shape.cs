namespace EngineeringTest.PhysicsEngine
{
    public class Shape
    {
        public float PosX; // X Position
        public float PosY; // Y Position 

        public Shape(float posX, float posY)
        {
            PosX = posX;
            PosY = posY;
        }
    }

}
