namespace EngineeringTest.PhysicsEngine
{
    public class Quadrilateral : Shape
    {
        public Quadrilateral(float posX, float posY) : base(posX, posY)
        {
            PosX = posX;
            PosY = posY;
        }
    }
}
